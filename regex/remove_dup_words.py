
###################################################
# Duplicate words 
# 
# The idea is to extract all the words separately from the string, create 
#empty array and compare each word with this array.
#If the word is not in the array, we add it there, otherwise skip it.
###################################################
import re

def remove_duplicates(text):
   #Apply the mask to find all separate words.
   text_list = re.findall(r"\w+", text)
   text_with_no_dup = []    
   
   #Compare each word in the array with the new, initially empty array.
   #If the word is not found in the array, it is added there.
   # Otherwise the word is skipped.
   for i in range(len(text_list)):
      if text_list[i] in text_with_no_dup:
         continue
      else:
         text_with_no_dup.append(text_list[i]) 
   
   #Create the string from the array without duplicate words.   
   text_with_no_dup = " ".join(text_with_no_dup)
       
   return text_with_no_dup

initial_string = "The Sky is blue also the ocean is blue also Rainbow has a blue colour."
         
print(remove_duplicates(initial_string))      