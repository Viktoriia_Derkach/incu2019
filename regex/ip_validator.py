
###################################################
# IP address validator 
# details: https://en.wikipedia.org/wiki/IP_address
# Student should enter function on the next lines.
# 
# 
# First function is_valid_ip() should return:
# True if the string inserted is a valid IP address
# or False if the string is not a real IP
# 
# 
# Second function get_ip_class() should return a string:
# "X is a class Y IP" or "X is classless IP" (without the ")
# where X represent the IP string, 
# and Y represent the class type: A, B, C, D, E
# ref: http://www.cloudtacker.com/Styles/IP_Address_Classes_and_Representation.png
# Note: if an IP address is not valid, the function should return
# "X is not a valid IP address"
###################################################

import re

def is_valid_ip(ip):
   #Here the mask is applied for the input string to validate the ip.
   pattern = re.compile("^\s*((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\.){3}(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])$")
   if pattern.match(ip) is not None:
      return True
   else:      
      return False

def get_ip_class(ip):
   if is_valid_ip(ip):
      ip_piece = ip.split('.')
      if int(ip_piece[0]) >= 0 and int(ip_piece[0]) <= 127:
         return (ip + " is a class A IP")  
      elif int(ip_piece[0]) >= 128 and int(ip_piece[0]) <= 191:
         return (ip + " is a class B IP")
      elif int(ip_piece[0]) >= 192 and int(ip_piece[0]) <= 223:
         return (ip + " is a class C IP")
      elif int(ip_piece[0]) >= 224 and int(ip_piece[0]) <= 239:
         return (ip + " is a class D IP")
      else:
         return (ip + " is a class E IP")
   else:
      return (ip + " is not a valid IP address")
    
ip="172.16.25.25"
print (is_valid_ip(ip))
print (get_ip_class(ip))