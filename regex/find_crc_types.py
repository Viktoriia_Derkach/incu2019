
###################################################
# Parse internal CRC counters of a Nexus
# The students will have to parse the next output, and return a dictionary,
# with the parsed info
# 
# How the output must look like:
#
# int_dict = {
# 	"Eth1/1": {
# 		"Align-Err": 0,
# 		"FCS-Err": 0,
# 		...
# 	}
# }
###################################################
import re
import json

def get_error_counters(text):
   #Get all the interfaces from the output and create a dictionary from them
   interfaces = re.findall(r"Eth\d\/\d", text)
   int_dict = dict.fromkeys(interfaces)
   
   #The mask is applied to extract all the counters.
   counters = re.findall(r"\b[^Ps\s]\w+\-?\w+(?<!1)\b", text)
   len_of_cnt = len(counters) 
   
   #Create the dictionary from the extracted counters for each interface
   for interface in int_dict:
      int_dict[interface] = dict.fromkeys(counters)
   
   #For each interface in the output
   for key in int_dict:
      value_corrected = []
      
      #Get array of strings corresponding to the specific interface's
      # counters values.
      value = re.findall(r"(?<="+key + ").+", text)
      
      #From all the strings in the value array, get the values of counters
      # and place them in the value_corrected array
      for string in value:
         string = re.findall(r"(\d+|\-\-)", string)
         value_corrected += string
      
      #If number of counters values obtained for the interface is less then
      #number of counters, put None value to the rest of counters.
      #P.S. Unfortunately, I don't know how to check which value
      #corresponds to which counter. Here I consider they rowly 
      #correspond to each other.
      if len(value_corrected) < len(counters):
         for i in range(len(counters) - len(value_corrected)):
            value_corrected.append("None")
      
      #Assign array with counters' values to the counters in the dictionary 
      for i in range(len(counters)):
         int_dict[key][counters[i]] = value_corrected[i]  
         
   return int_dict

text = """
 --------------------------------------------------------------------------------
 Port          Align-Err    FCS-Err   Xmit-Err    Rcv-Err  UnderSize OutDiscards
 --------------------------------------------------------------------------------
 Eth1/1                0          0          0          0          0           0
 Eth1/2                0          0          0          0          0           0
 Eth1/3                0          3          0         26          0           0
 Eth1/4                0          0          0          0          0           0
 <snip>

 --------------------------------------------------------------------------------
 Port         Single-Col  Multi-Col   Late-Col  Exces-Col  Carri-Sen       Runts
 --------------------------------------------------------------------------------
 Eth1/1                0          0         15          0          0           0
 Eth1/2                0          0          0          0          0           0
 <snip>

 --------------------------------------------------------------------------------
 Port          Giants SQETest-Err Deferred-Tx IntMacTx-Er IntMacRx-Er Symbol-Err
 --------------------------------------------------------------------------------
 Eth1/1             0          --           0         128           0          0
 Eth1/2             0          --           0           0           0          0"""
 
print("int_dict = ", json.dumps(get_error_counters(text), indent=4))