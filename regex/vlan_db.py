
###################################################
# Parse vlan status
# 
# Students will need to parse the output of a "show vlan" from a NXOS devices
# and return a dictionary with the parsed info
#
# How the output must look like:
# 
# vlan_db = {
# 	"1": {
# 		"Name": "default",
# 		"Status": "active",,
# 		"Ports": ["Po213", "Eth1/1"...]
# 	},
# 	"2":{},
# 	...
# }
#
###################################################
import re
import json

def get_vlan_db(text):
   #Get the vlan numbers and limit the possible number
   #to the range from 0 to 4095. Create dictionary from those numbers.
   vlan_num = re.findall(r"\b(?<!\/)(?<!\w)([0-9]|[1-9]\d|[1-9]\d{2}|[1-3]\d{3}|40[0-9][0-5])\b", text)
   vlan_db = dict.fromkeys(vlan_num)

   #Get all the characteristics (such as Name, Status, etc) which go 
   #after VLAN word in the string. Add them as a dictionary to each vlan.
   NSP = re.search(r"(?<=VLAN).+", text)
   NSP = NSP.group()
   NSP = re.findall(r"\b\w+\b", NSP)
   for vlan in vlan_db:
      vlan_db[vlan] = dict.fromkeys(NSP)

   #Here the idea is to get all the info which corresponds to each vlan.If vlan
   #is not the last one in the list, then the mask is applied to get all the
   #lines between that and next vlan from the list (e.g. between 1st and 10th 
   #vlans). If the interested vlan is the last one in the list, then all the 
   #strings to the end of the output are considered to pertain to that vlan.
   for i in range(len(vlan_num)):
      if vlan_num[i] != vlan_num[-1]:
         info = re.findall(r"(?<=" +vlan_num[i] + "\s).+(?=\s" +vlan_num[i+1] + "\s)", text, re.DOTALL)
      else:
         info = re.findall(r"(?<=" +vlan_num[i] + "\s).+", text, re.DOTALL)
         
      #Extract all the words without \n sign from the returned strings.Because 
      #findall() returns array, info[0] is used.
      info = re.findall(r'\b[^n]\w+\-?\/?\w+\b', info[0])
      
      #Apply obtained values to the characteristics. Here I consider that ports
      #are placed at the end. That is why it is checked if the characterostoc is
      #the last one and if it is, all remained info is applied to it. 
      for j in range(len(NSP)):
         if NSP[j] != NSP[-1]:
            vlan_db[vlan_num[i]][NSP[j]] = info[j]
         else:
            vlan_db[vlan_num[i]][NSP[j]] = info[j:]	
            
   return vlan_db

text = """ 
VLAN Name                             Status    Ports
 ---- -------------------------------- --------- -------------------------------
 1    default                          active    Po213, Eth1/1, Eth1/2, Eth1/3
                                                 Eth1/4
 10   Vlan10                           active    Po1, Po10, Po111, Po213, Eth1/2
                                                 Eth1/5, Eth1/16, Eth1/17
                                                 Eth1/18, Eth1/49, Eth1/50
 14   Vlan14                           active    Po213, Eth1/1, Eth1/2, Eth1/3
                                                 Eth1/4                 """
         
print("vlan_db =", json.dumps(get_vlan_db(text), indent=4))
