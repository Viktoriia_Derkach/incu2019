##Import the necessary libraries
import requests
import json

#Define the class named nexus(). 
 
class nexus():
   
#create the dictionaty with all the access details 
   
    online_nexus = {"ip": "sbx-nxos-mgmt.cisco.com", 
                "port": "80", 
                "user":"admin", 
                "pass":"Admin_1234!"}


    uri = 'http://{}:{}/ins'.format(online_nexus['ip'], online_nexus['port'])

    jsonrpc_headers = {'Content-Type': 'application/json-rpc'}


#show version command is used in payload to request version and platform 
#details which then are stored in the correspondent variables 
    
    payload = [
      {
         "jsonrpc": "2.0",
         "method": "cli",
         "params": {
            "cmd": "show version",
            "version": 1
         },
         "id": 1
       }
      ]
   
    response = requests.post(uri,
                            data=json.dumps(payload),
                            headers=jsonrpc_headers, 
                            auth=(online_nexus["user"], online_nexus["pass"]))
    
    response_dic = json.loads(response.text)
    
    version = response_dic["result"]["body"]["kickstart_ver_str"]
    platform = response_dic["result"]["body"]["chassis_id"]
 
#define the method to get the interface status. Interface name is used as
#an input argument and returns the state of the interface   
    
    def get_interface_status(self, if_name):
        payload_get = [
          {
            "jsonrpc": "2.0",
            "method": "cli",
            "params": {
               "cmd": "show interface ",
               "version": 1
            },
            "id": 1
          }
         ]

#here we add the interface name to the cmd key in order to get valid command
#for the desired interface  and make the request      
       
        payload_get[0]["params"]["cmd"] += if_name   
        
        response = requests.post(uri,
                            data=json.dumps(payload_get),
                            headers=jsonrpc_headers, 
                            auth=(online_nexus["user"], online_nexus["pass"]))

#convert the response into dictionary and save interface status key value
#to the if_status variable  
        
        response_dic = json.loads(response.text) 
        if_status = response_dic["result"]["body"]["TABLE_interface"]
                                ["ROW_interface"]["state"]

#make the verification that interface state is either up, down or unknown 
                                
        if if_status == "up" or if_status == "down":
           return if_status
        else:
           return  "unknown"

#define method to change the interface description. Method takes interface 
#name and interface descripiton as input argumants and doesn't return anything
           
    def configure_interface_desc(self, if_name, if_desc):
        payload_desc = [
          {
            "jsonrpc": "2.0",
            "method": "cli",
            "params": {
               "cmd": "interface ",
               "version": 1
            },
            "id": 1
            },
            {
            "jsonrpc": "2.0",
            "method": "cli",
            "params": {
               "cmd": "description ",
               "version": 1
            },
            "id": 2
          }
         ]

#add the desired interface name and description to the payload cmd key
#in order to get valid commands and make the request      
       
        payload_desc[0]["params"]["cmd"] += if_name 
        payload_desc[1]["params"]["cmd"] += if_desc
                
        response = requests.post(uri,
                            data=json.dumps(payload_desc),
                            headers=jsonrpc_headers, 
                            auth=(online_nexus["user"], online_nexus["pass"]))