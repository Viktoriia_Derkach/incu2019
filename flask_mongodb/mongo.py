from pymongo import MongoClient
from bson.json_util import dumps


def get_mongo():
    client   = MongoClient("localhost", 27017)
    db = client["incubator"]
    return db['working address book']

def insert_new_entry(dict):
    db = get_mongo()
    action = db.insert_one(dict)

def update_entry(firstname, lastname, dict):
    db = get_mongo()
    action = db.update_many({"First Name" : str(firstname) , "Last Name" : str(lastname)}, dict)

def display_entries(firstname = None, lastname = None):
    db = get_mongo()
    if firstname != None and lastname != None:
        action = db.find({"First Name" : {'$regex' : str(firstname)} , "Last Name" : {'$regex': str(lastname)}})
    else:
        action = db.find()
        action = sorted(action, key = lambda k: k["Last Name"])

    for entries in action:
         print(dumps(entries))


def delete_entries(firstname = None, lastname = None):
    db = get_mongo()
    if firstname != None and lastname != None:
        action = db.delete_many({"First Name" : str(firstname) , "Last Name" : str(lastname)})
    else:
        action = db.delete_many({})



#Checking 

book = [
{	
    "First Name" : "Viktoriia", 
    "Last Name" : "Derkach",
    "Email address" : "dekjhfdb@gmail.com",
    "Home phone number" : "0666515185",
    "Work phone number" : "0963651484"
},
{
    "First Name" : "Julia", 
    "Last Name" : "Hydcv",
    "Email address" : "hdguk95@gmail.com",
    "Home phone number" : "0666254100",
    "Work phone number" : "0963365581"
},
{
    "First Name" : "John", 
    "Last Name" : "Vgsfdvbv",
    "Email address" : "john@gmail.com",
    "Home phone number" : "0952333331",
    "Work phone number" : "0502455557"
}
]

update = {'$set':{"Home phone number":'23658'}}

first = "Vi"
second = "Der"
delete_entries()

insert_new_entry(book)
display_entries()

    