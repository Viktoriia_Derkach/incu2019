# -*- coding: utf-8 -*-

from flask import Flask
from flask import request
from flask import abort
from flask import jsonify

app = Flask(__name__)

address_book = [
      {
            "Userid": 1,
            "Name":"Viktoriia",
            "Phone number":"0669264049",
            "Email":"derkachviktoriia@gmail.com"
      },
      {
            "Userid": 2,
            "Name":"Julia",
            "Phone number":"0669264049",
            "Email":"derkachviktoriia@gmail.com"
      },
      {
            "Userid": 3,
            "Name":"Irina",
            "Phone number":"0669264049",
            "Email":"derkachviktoriia@gmail.com"
      }
]

#List all elements of address book 
@app.route("/book", methods = ['GET'])
def return_all():
    return jsonify({'Address book': address_book})

#List a single element of the address book identified by id 
@app.route('/book/find/<int:user_id>', methods = ['GET'])  
def return_one(user_id):
   for user in address_book:
      if user['Userid']==user_id:
         return jsonify({'User':user})
      
#Creates a new element      
@app.route('/book/new', methods = ['POST'])   
def new_element():
   user_info = request.get_json()
   address_book.append(user_info)
   return "You added new user: %s" %request.get_json()['Name']

#Edit a single element identified by id 
@app.route('/book/update/<int:user_id>', methods = ['PUT'])
def update(user_id):
   if request.get_json() is not None:
      for user in address_book:
         if user['Userid']==user_id:
            user['Name'] = request.json.get('Name', user['Name'])
            user['Phone number'] = request.json.get('Phone number', user['Phone number'])
            user['Email'] = request.json.get('Email', user['Email'])
      return  jsonify({'Updated': user})      
   else:
      return  abort(406)
             
#Deletes a single element identified by id    
@app.route('/book/delete/<int:user_id>', methods = ['DELETE'])
def delete(user_id):
   for user in address_book:
      if user['Userid']==user_id:
         address_book.remove(user)
   return jsonify({'Updated':address_book})      
   
app.run(port=7676, debug=True)